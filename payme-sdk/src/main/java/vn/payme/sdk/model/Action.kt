package vn.payme.sdk.model

enum class Action {
    DEPOSIT, PAY, OPEN,WITHDRAW,TEST
}