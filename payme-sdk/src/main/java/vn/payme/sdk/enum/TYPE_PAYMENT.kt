package vn.payme.sdk.enum

class TYPE_PAYMENT {
    companion object{
        val APP_WALLET :String = "AppWallet"
        val NAPAS :String = "Napas"
        val PVCB :String = "PVCBank"
    }
}